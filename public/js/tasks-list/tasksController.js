/**
 * @author :: Elodie Bayet
 *   @role :: Fullstack Web Developer
 *   @date :: 2019.10 + 2022.03
 * @origin :: Belgium, EU
 */

/**
 * Manage and bind View and Data of a Tasks List 
 * @param {Object} model Classe to manage Data
 * @param {Object} view Classe to handle view
 */
class TasksController {

	constructor(model, view) {
		this._model = model;
		this._view = view;
		
		this._onListChanged(this._model._list); // ?!...meh

		this._view.bindAddTask( this._handleAddTask );
		this._view.bindDeleteTask( this._handleDeleteTask );
		this._view.bindEditTask( this._handleEditTask );
		this._view.bindCompleteTask( this._handleCompleteTask );

		this._model.bindListChanged( this._onListChanged );
	}

	// Model-to-View bindings --

	/**  */
	_onListChanged = list => {
		this._view.buildList(list);
	}

	// View-to-Model bindings --

	/**  */
	_handleAddTask = label => {
		this._model.addTask(label);
	}
	/**  */
	_handleDeleteTask = id => {
		this._model.deleteTask(id);
	}
	/**  */
	_handleEditTask = (id, label) => {
		this._model.editTask(id, label);
	}
	/**  */
	_handleCompleteTask = id => {
		this._model.completeTask(id);
	}
}

export default TasksController;