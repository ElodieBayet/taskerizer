/**
 * @author :: Elodie Bayet
 *   @role :: Fullstack Web Developer
 *   @date :: 2019.10 + 2022.03
 * @origin :: Belgium, EU
 */

/**
 * Manage list of tasks :
 * * add a task
 * * delete a task
 * * edit a task
 * * complete a task
 */
class TasksModel {

	constructor() {
		// Fake list
		this._list = [
			{id: 1, label: "Rédiger la documentation", complete: false},
			{id: 2, label: "Publier le code de l'application", complete: true},
			{id: 3, label: "Déployer une démonstration", complete: false}
		]; // Or from long-term storage: JSON.parse( localStorage.getItem('tasks') ) || [];
	}

	/**
	 * Inserts a task at the end of the list
	 * @param {string} label Description of task
	 */
	addTask(label) {
		const task = {
			id : this._list.length > 0 ? this._list[this._list.length - 1].id + 1 : 1,
			label : label,
			complete : false
		}
		this._list.push(task);
		this._listChanged(this._list);
		// Or for long-term storage: this._commit(this._list);
	}

	/**
	 * Removes task by id
	 * @param {number} id Unique number of task
	 */
	deleteTask(id) {
		this._list = this._list.filter( task => { return task.id !== id } );
		this._listChanged(this._list); 
	}

	/**
	 * Modifies a task by id
	 * @param {number} id Unique number of task
	 * @param {string} newLabel New description of task
	 */
	editTask(id, newLabel) {	
		this._list = this._list.map( task => {
			return task.id === id ? {id: task.id, label: newLabel, complete: task.complete} : task
		} );
		this._listChanged(this._list); 
	}

	/**
	 * Marks a task as 'complete' by id 
	 * @param {number} id Unique number of task
	 */
	completeTask(id) {
		this._list = this._list.map( task => {
			return task.id === id ? {id: task.id, label: task.label, complete: !task.complete} : task
		} );
		this._listChanged(this._list); 
	}

	/**
	 * Binds every action on list
	 * @param {Function} callback 
	 */
	bindListChanged(callback) {
		this._listChanged = callback;
	}

	/**
	 * Store tasks list for long-term
	 * @param {Array} tasks List of tasks
	 */
	_commit(tasks) {
		this.onListChange(tasks);
		localStorage.setItem('tasks', JSON.stringify(tasks));
	}
}

export default TasksModel;