/**
 * @author :: Elodie Bayet
 *   @role :: Fullstack Web Developer
 *   @date :: 2019.10 + 2022.03
 * @origin :: Belgium, EU
 */

/**
 * Handle HTML display for Tasks List mangement
 * @param {Object} form HTML form element
 * @param {Object} list HTML list element
 * @param {Object} domBuilder Instance of class for HTML node builder
 * @param {(string|null)} query Optional. Identifier of input field for adding task if more of 2 text fields
 */
class TasksView {

	constructor(form, list, domBuilder, inputId = null) {

		this._form = form;
		this._list = list;

		let id = inputId || 'input[type="text"]';
		this._input = form.querySelector(id);
		this._submit = form.querySelector('input[type="submit"]');

		// HTML node builder
		this._dom = domBuilder;
		
		// Temporary task storage when edit label
		this._tempEdit = '';
		
		// Start
		this._uiEventsManager();
	}

	/**
	 * Extract unique number of a task
	 * @param {string} id 
	 * @returns {number}
	 */
	_getIntegerId(id) {
		return parseInt(id.replace('item', ''));
	}

	/**
	 * Enable local events for UI interactions
	 */
	_uiEventsManager() {
		this._list.addEventListener('input', evt => {
			if (evt.target.className === 'item') {
				this._tempEdit = evt.target.textContent;
			}
		});
		this._input.addEventListener('input', evt => {
			if (evt.target.value) {
				this._submit.disabled = false;
			} else {
				this._submit.disabled = true;
			}
		})
	}

	/**
	 * Build or Re-Build HTML list of tasks when item added, edited, completed, or deleted
	 * @param {Array} list Tasks list sent from model
	 */
	buildList(list) {
		
		this._list.innerHTML = '';
		
		if (list.length === 0) {
			
			this._list.append(this._dom.nodeCreator('li', "Aucune tâche trouvée", {className:'nothing'}));
		} else {
			
			list.forEach( task => {
				let span = {};
				let attributes = {type: "checkbox", name: `chkT${task.id}`};

				if (task.complete) {
					
					span = this._dom.nodeCreator('span', this._dom.nodeCreator('s', task.label, null), {contentEditable: true});
					attributes['checked'] = true;

				} else {
					span = this._dom.nodeCreator('span', task.label, {'contentEditable': true, 'class':'item'});
				}

				const li = this._dom.nodeCreator('li', [
					this._dom.nodeCreator('input', null, attributes),
					span,
					this._dom.nodeCreator('button', 'Supprimer', {'class': 'delete button'})
				], {id:`item${task.id}`});
			
				this._list.append(li);
			} )	
		}
	}

	bindAddTask(handler){
		this._form.addEventListener('submit', evt => {
			evt.preventDefault();
			if (this._input.value) {
				handler(this._input.value);
				this._input.value = '';
			}
		})
	} 

	bindDeleteTask(handler){
		this._list.addEventListener('click', evt => {
			if (evt.target.classList.contains('delete')) {
				const id = this._getIntegerId(evt.target.parentElement.id);
				handler(id);
			}
		})
	}

	bindEditTask(handler){
		this._list.addEventListener('focusout', evt => {
			if (this._tempEdit) {
				const id = this._getIntegerId(evt.target.parentElement.id);
				handler(id, this._tempEdit);
				this._tempEdit = '';
			}
		})
	}

	bindCompleteTask(handler){
		this._list.addEventListener('change', evt => {
			if (evt.target.type === 'checkbox'){
				const id = this._getIntegerId(evt.target.parentElement.id);
				handler(id);
			}
		})
	}
}

export default TasksView;