'use strict';
/**
 * @author :: Elodie Bayet
 *   @role :: Fullstack Web Developer
 *   @date :: 2019.10 + 2022.03
 * @origin :: Belgium, EU
 */

import domBuilder from '../../public/js/lib/domBuilder.js';
import TasksController from './tasks-list/tasksController.js';
import TasksModel from './tasks-list/tasksModel.js';
import TasksView from './tasks-list/tasksView.js';

(() => {
	const form = document.querySelector('form[name="taskform"]');
	const list = document.querySelector('#tasksList');

	const model = new TasksModel();
	const view = new TasksView(form, list, domBuilder);

	const tasks = new TasksController(model, view);
})();