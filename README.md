# Taskerizer :: Tasks List

Date | Reviewed | Version | Purpose | Live
---- | -------- | ------- | ------- | ----
2019.10 | 2022.03 | 1.1.0 | Demo | [Taskerizer](https://demo.elodiebayet.com/taskerizer)


## Sommaire 
0. [Presentation](#markdown-header-0-presentation)
    * [0.0 - Technologies](#markdown-header-00-technologies)
    * [0.1 - Features](#markdown-header-01-features)
    * [0.2 - Usage](#markdown-header-02-usage)
1. [Structure](#markdown-header-1-structure)
    * [1.0 - Folders and files](#markdown-header-10-folders-and-files)
    * [1.1 - Interface](#markdown-header-11-interface)
2. [Development](#markdown-header-2-development)
3. [Remarks](#markdown-header-3-remarks)

---

## 0 - Presentation

JavaScript Single-Page Application of a tasks list manager. Adding, deleting, modifying and finalizing tasks.


### 0.0 - Technologies

* [JavaScript](https://developer.mozilla.org/fr/docs/Web/JavaScript)


### 0.1 - Features

* **Usual manipulations** _adding, modifying, deleting, or completing tasks_


### 0.2 - Usage

This project is just a _demo_. It doesn't fit for public or professionnal usage. Reproduction, public communication, partial or entire of this Website or its content are strictly prohibited without an official and written authorization by the Developer.

---

## 1 - Structure

### 1.0 - Folders and files

* `public/` _public source code for development_
    - `css/` 
    - `js/`
* `assets/`
    - `figures/` _global images or pictures for content integration_
    - `trademark/` _logos, icons, etc. relative to owner / brand_


### 1.1 - Interface

![Screenshot v1.1](./assets/figures/screenshot_v1-1.jpg)

---

## 2 - Development

Architecture pattern in _Model-View-Controller_. JavaScript ES6 syntax.

---

## 3 - Remarks

None.